﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearch
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSearch))
        Me.cbxSort = New System.Windows.Forms.ComboBox()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.dgdSearch = New System.Windows.Forms.DataGridView()
        Me.NoClothesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateAcceptDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AddressDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MobileDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WeightClothesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PriceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TimeAcceptDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LaundryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LaundryShopDataSet = New LaundryShopSystemV1._1.LaundryShopDataSet()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.LaundryTableAdapter = New LaundryShopSystemV1._1.LaundryShopDataSetTableAdapters.LaundryTableAdapter()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.dtpCalendar = New System.Windows.Forms.DateTimePicker()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtMobile = New System.Windows.Forms.TextBox()
        Me.txtNoClothes = New System.Windows.Forms.TextBox()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.lblPrice = New System.Windows.Forms.Label()
        Me.txtWeClothes = New System.Windows.Forms.TextBox()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.cbxStatus = New System.Windows.Forms.ComboBox()
        CType(Me.dgdSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LaundryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LaundryShopDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbxSort
        '
        Me.cbxSort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSort.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSort.FormattingEnabled = True
        Me.cbxSort.Items.AddRange(New Object() {"VIEW ALL", "ON GOING", "FINISH", "CLAIM"})
        Me.cbxSort.Location = New System.Drawing.Point(801, 28)
        Me.cbxSort.Name = "cbxSort"
        Me.cbxSort.Size = New System.Drawing.Size(94, 26)
        Me.cbxSort.TabIndex = 11
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(597, 28)
        Me.txtSearch.MaxLength = 11
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(97, 26)
        Me.txtSearch.TabIndex = 10
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Location = New System.Drawing.Point(238, 690)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 28)
        Me.btnEdit.TabIndex = 7
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'dgdSearch
        '
        Me.dgdSearch.AutoGenerateColumns = False
        Me.dgdSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgdSearch.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NoClothesDataGridViewTextBoxColumn, Me.CoNameDataGridViewTextBoxColumn, Me.DateAcceptDataGridViewTextBoxColumn, Me.StatusDataGridViewTextBoxColumn, Me.IDDataGridViewTextBoxColumn, Me.AddressDataGridViewTextBoxColumn, Me.MobileDataGridViewTextBoxColumn, Me.WeightClothesDataGridViewTextBoxColumn, Me.PriceDataGridViewTextBoxColumn, Me.TimeAcceptDataGridViewTextBoxColumn})
        Me.dgdSearch.DataSource = Me.LaundryBindingSource
        Me.dgdSearch.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgdSearch.Location = New System.Drawing.Point(506, 73)
        Me.dgdSearch.Name = "dgdSearch"
        Me.dgdSearch.Size = New System.Drawing.Size(545, 596)
        Me.dgdSearch.TabIndex = 0
        '
        'NoClothesDataGridViewTextBoxColumn
        '
        Me.NoClothesDataGridViewTextBoxColumn.DataPropertyName = "No_Clothes"
        Me.NoClothesDataGridViewTextBoxColumn.HeaderText = "No_Clothes"
        Me.NoClothesDataGridViewTextBoxColumn.Name = "NoClothesDataGridViewTextBoxColumn"
        '
        'CoNameDataGridViewTextBoxColumn
        '
        Me.CoNameDataGridViewTextBoxColumn.DataPropertyName = "CoName"
        Me.CoNameDataGridViewTextBoxColumn.HeaderText = "CoName"
        Me.CoNameDataGridViewTextBoxColumn.Name = "CoNameDataGridViewTextBoxColumn"
        '
        'DateAcceptDataGridViewTextBoxColumn
        '
        Me.DateAcceptDataGridViewTextBoxColumn.DataPropertyName = "DateAccept"
        Me.DateAcceptDataGridViewTextBoxColumn.HeaderText = "DateAccept"
        Me.DateAcceptDataGridViewTextBoxColumn.Name = "DateAcceptDataGridViewTextBoxColumn"
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.Visible = False
        '
        'AddressDataGridViewTextBoxColumn
        '
        Me.AddressDataGridViewTextBoxColumn.DataPropertyName = "Address"
        Me.AddressDataGridViewTextBoxColumn.HeaderText = "Address"
        Me.AddressDataGridViewTextBoxColumn.Name = "AddressDataGridViewTextBoxColumn"
        Me.AddressDataGridViewTextBoxColumn.Visible = False
        '
        'MobileDataGridViewTextBoxColumn
        '
        Me.MobileDataGridViewTextBoxColumn.DataPropertyName = "Mobile"
        Me.MobileDataGridViewTextBoxColumn.HeaderText = "Mobile"
        Me.MobileDataGridViewTextBoxColumn.Name = "MobileDataGridViewTextBoxColumn"
        Me.MobileDataGridViewTextBoxColumn.Visible = False
        '
        'WeightClothesDataGridViewTextBoxColumn
        '
        Me.WeightClothesDataGridViewTextBoxColumn.DataPropertyName = "Weight_Clothes"
        Me.WeightClothesDataGridViewTextBoxColumn.HeaderText = "Weight_Clothes"
        Me.WeightClothesDataGridViewTextBoxColumn.Name = "WeightClothesDataGridViewTextBoxColumn"
        Me.WeightClothesDataGridViewTextBoxColumn.Visible = False
        '
        'PriceDataGridViewTextBoxColumn
        '
        Me.PriceDataGridViewTextBoxColumn.DataPropertyName = "Price"
        Me.PriceDataGridViewTextBoxColumn.HeaderText = "Price"
        Me.PriceDataGridViewTextBoxColumn.Name = "PriceDataGridViewTextBoxColumn"
        '
        'TimeAcceptDataGridViewTextBoxColumn
        '
        Me.TimeAcceptDataGridViewTextBoxColumn.DataPropertyName = "TimeAccept"
        Me.TimeAcceptDataGridViewTextBoxColumn.HeaderText = "TimeAccept"
        Me.TimeAcceptDataGridViewTextBoxColumn.Name = "TimeAcceptDataGridViewTextBoxColumn"
        Me.TimeAcceptDataGridViewTextBoxColumn.Visible = False
        '
        'LaundryBindingSource
        '
        Me.LaundryBindingSource.DataMember = "Laundry"
        Me.LaundryBindingSource.DataSource = Me.LaundryShopDataSet
        '
        'LaundryShopDataSet
        '
        Me.LaundryShopDataSet.DataSetName = "LaundryShopDataSet"
        Me.LaundryShopDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnBack
        '
        Me.btnBack.BackColor = System.Drawing.Color.White
        Me.btnBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBack.Location = New System.Drawing.Point(400, 690)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 28)
        Me.btnBack.TabIndex = 9
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = False
        '
        'LaundryTableAdapter
        '
        Me.LaundryTableAdapter.ClearBeforeFill = True
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(319, 690)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 28)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'dtpCalendar
        '
        Me.dtpCalendar.CalendarForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.dtpCalendar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCalendar.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCalendar.Location = New System.Drawing.Point(922, 28)
        Me.dtpCalendar.MaxDate = New Date(2095, 12, 31, 0, 0, 0, 0)
        Me.dtpCalendar.Name = "dtpCalendar"
        Me.dtpCalendar.Size = New System.Drawing.Size(113, 29)
        Me.dtpCalendar.TabIndex = 12
        '
        'txtAddress
        '
        Me.txtAddress.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LaundryBindingSource, "Address", True))
        Me.txtAddress.Enabled = False
        Me.txtAddress.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(216, 167)
        Me.txtAddress.MaxLength = 500
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(250, 26)
        Me.txtAddress.TabIndex = 2
        '
        'txtMobile
        '
        Me.txtMobile.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LaundryBindingSource, "Mobile", True))
        Me.txtMobile.Enabled = False
        Me.txtMobile.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobile.Location = New System.Drawing.Point(216, 232)
        Me.txtMobile.MaxLength = 11
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.Size = New System.Drawing.Size(250, 26)
        Me.txtMobile.TabIndex = 3
        '
        'txtNoClothes
        '
        Me.txtNoClothes.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LaundryBindingSource, "No_Clothes", True))
        Me.txtNoClothes.Enabled = False
        Me.txtNoClothes.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoClothes.Location = New System.Drawing.Point(216, 299)
        Me.txtNoClothes.MaxLength = 3
        Me.txtNoClothes.Name = "txtNoClothes"
        Me.txtNoClothes.Size = New System.Drawing.Size(68, 26)
        Me.txtNoClothes.TabIndex = 4
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.BackColor = System.Drawing.Color.Transparent
        Me.lblDate.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LaundryBindingSource, "DateAccept", True))
        Me.lblDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblDate.Location = New System.Drawing.Point(211, 502)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(85, 29)
        Me.lblDate.TabIndex = 69
        Me.lblDate.Text = "            "
        '
        'txtName
        '
        Me.txtName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LaundryBindingSource, "CoName", True))
        Me.txtName.Enabled = False
        Me.txtName.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(216, 102)
        Me.txtName.MaxLength = 500
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(250, 26)
        Me.txtName.TabIndex = 1
        '
        'lblPrice
        '
        Me.lblPrice.AutoSize = True
        Me.lblPrice.BackColor = System.Drawing.Color.Transparent
        Me.lblPrice.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LaundryBindingSource, "Price", True))
        Me.lblPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrice.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblPrice.Location = New System.Drawing.Point(211, 437)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(85, 29)
        Me.lblPrice.TabIndex = 63
        Me.lblPrice.Text = "            "
        '
        'txtWeClothes
        '
        Me.txtWeClothes.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LaundryBindingSource, "Weight_Clothes", True))
        Me.txtWeClothes.Enabled = False
        Me.txtWeClothes.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeClothes.Location = New System.Drawing.Point(216, 373)
        Me.txtWeClothes.MaxLength = 5
        Me.txtWeClothes.Name = "txtWeClothes"
        Me.txtWeClothes.Size = New System.Drawing.Size(44, 26)
        Me.txtWeClothes.TabIndex = 5
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.BackColor = System.Drawing.Color.Transparent
        Me.lblTime.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LaundryBindingSource, "TimeAccept", True))
        Me.lblTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTime.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblTime.Location = New System.Drawing.Point(211, 564)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(85, 29)
        Me.lblTime.TabIndex = 68
        Me.lblTime.Text = "            "
        '
        'cbxStatus
        '
        Me.cbxStatus.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LaundryBindingSource, "Status", True))
        Me.cbxStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxStatus.Enabled = False
        Me.cbxStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStatus.FormattingEnabled = True
        Me.cbxStatus.Items.AddRange(New Object() {"On Going", "Finish", "Claimed"})
        Me.cbxStatus.Location = New System.Drawing.Point(216, 635)
        Me.cbxStatus.Name = "cbxStatus"
        Me.cbxStatus.Size = New System.Drawing.Size(99, 26)
        Me.cbxStatus.TabIndex = 6
        '
        'frmSearch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1264, 730)
        Me.Controls.Add(Me.lblDate)
        Me.Controls.Add(Me.dtpCalendar)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.cbxStatus)
        Me.Controls.Add(Me.dgdSearch)
        Me.Controls.Add(Me.txtNoClothes)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.txtMobile)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.cbxSort)
        Me.Controls.Add(Me.txtWeClothes)
        Me.Controls.Add(Me.txtSearch)
        Me.Controls.Add(Me.txtAddress)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.lblPrice)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSearch"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmSearch"
        CType(Me.dgdSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LaundryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LaundryShopDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxSort As System.Windows.Forms.ComboBox
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents dgdSearch As System.Windows.Forms.DataGridView
    Friend WithEvents LaundryShopDataSet As LaundryShopSystemV1._1.LaundryShopDataSet
    Friend WithEvents LaundryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LaundryTableAdapter As LaundryShopSystemV1._1.LaundryShopDataSetTableAdapters.LaundryTableAdapter
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents dtpCalendar As System.Windows.Forms.DateTimePicker
    Friend WithEvents NoClothesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateAcceptDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddressDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MobileDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WeightClothesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PriceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TimeAcceptDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtMobile As System.Windows.Forms.TextBox
    Private WithEvents txtNoClothes As System.Windows.Forms.TextBox
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents lblPrice As System.Windows.Forms.Label
    Friend WithEvents txtWeClothes As System.Windows.Forms.TextBox
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents cbxStatus As System.Windows.Forms.ComboBox
End Class
