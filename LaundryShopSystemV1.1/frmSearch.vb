﻿Imports System.Data.OleDb
Public Class frmSearch 
    Dim dbPath As String = Application.StartupPath & "\LaundryShop.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath
    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing
        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
        Return Nothing
    End Function
    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try
        Return True
    End Function
    Private Function EnableFields()
        txtName.Enabled = True
        txtAddress.Enabled = True
        txtMobile.Enabled = True
        txtNoClothes.Enabled = True
        txtWeClothes.Enabled = True
        cbxStatus.Enabled = True
    End Function
    Private Function disableFields()
        txtName.Enabled = False
        txtAddress.Enabled = False
        txtMobile.Enabled = False
        txtNoClothes.Enabled = False
        txtWeClothes.Enabled = False
        cbxStatus.Enabled = False
    End Function
    Private Sub frmSearch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'LaundryShopDataSet.Laundry' table. You can move, or remove it, as needed.
        Me.LaundryTableAdapter.Fill(Me.LaundryShopDataSet.Laundry)
        'TODO: This line of code loads data into the 'LaundryShopDataSet.Laundry' table. You can move, or remove it, as needed.
        Me.LaundryTableAdapter.Fill(Me.LaundryShopDataSet.Laundry)
    End Sub
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Select Case btnEdit.Text
            Case "Edit"
                EnableFields()
                txtSearch.Enabled = False
                cbxSort.Enabled = False
                dtpCalendar.Enabled = False
                dgdSearch.Enabled = False
                btnEdit.Text = "Update"
            Case "Update"
                disableFields()
                txtSearch.Enabled = True
                cbxSort.Enabled = True
                dtpCalendar.Enabled = True
                dgdSearch.Enabled = True
                btnEdit.Text = "Edit"
                On Error GoTo SaveErr
                LaundryBindingSource.EndEdit()
                LaundryTableAdapter.Update(LaundryShopDataSet.Laundry)
                MsgBox("Information Has Been Updated")
ErrEx:
                Exit Sub
SaveErr:
        End Select
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxSort.SelectedIndexChanged
        Me.LaundryBindingSource.Filter = "Status = '" & cbxSort.Text & "'"
        If cbxSort.Text = "VIEW ALL" Then
            On Error GoTo ErrRe
            txtSearch.Select()
            LaundryBindingSource.Filter = Nothing
            With dgdSearch
                .ClearSelection()
                .ReadOnly = True
                .MultiSelect = False
                .DataSource = LaundryBindingSource
            End With

            Me.reset()

ErrEx:
            Exit Sub
ErrRe:
            MsgBox("Error Number " & Err.Number & vbNewLine & _
                   "Error Description " & Err.Description, MsgBoxStyle.Critical, _
                   "Reset Error!")
            Resume ErrEx

        End If

    End Sub
    Private Sub txtWeClothes_TextChanged(sender As Object, e As EventArgs) Handles txtWeClothes.TextChanged
        Try
            If txtWeClothes.Text.Length > 0 Then
                lblPrice.Text = CDbl(txtWeClothes.Text) * 25
            Else
                lblPrice.Text = String.Empty
            End If
        Catch
        End Try
    End Sub
    Private Sub txtWeClothes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWeClothes.KeyPress
        If Not Char.IsControl(e.KeyChar) AndAlso Not Char.IsDigit(e.KeyChar) AndAlso e.KeyChar <> "."c Then
            e.Handled = True
        End If
        If e.KeyChar = "."c AndAlso TryCast(sender, TextBox).Text.IndexOf("."c) > -1 Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtNoClothes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNoClothes.KeyPress
         If Not Char.IsControl(e.KeyChar) AndAlso Not Char.IsDigit(e.KeyChar) AndAlso e.KeyChar <> "."c Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtMobile_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMobile.KeyPress
        If Not Char.IsControl(e.KeyChar) AndAlso Not Char.IsDigit(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtName.KeyPress
        If Not Char.IsControl(e.KeyChar) AndAlso Not Char.IsLetter(e.KeyChar) 
            e.Handled = True
        End If
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Me.LaundryBindingSource.Filter = "CoName like '%" & txtSearch.Text & "%'"

    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Select Case btnBack.Text
            Case "Back"
                disableFields()
                txtSearch.Enabled = True
                cbxSort.Enabled = True
                dtpCalendar.Enabled = True
                dgdSearch.Enabled = True
                btnEdit.Text = "Edit"
                frmMain.Show()
                Me.Hide()
                frmMain.cbxStatus.Text = "ON GOING"
        End Select
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Select Case btnCancel.Text
            Case "Cancel"
                disableFields()
                txtSearch.Enabled = True
                cbxSort.Enabled = True
                dtpCalendar.Enabled = True
                dgdSearch.Enabled = True
                btnEdit.Text = "Edit"
        End Select
    End Sub
    Private Sub reset()
        Dim txtS As TextBox = txtSearch
        With txtS
            .Text = ""
            .Select()
        End With
        If dgdSearch.DataSource Is Nothing Then
            Exit Sub
        End If
    End Sub
    Private Sub dtpCalendar_ValueChanged(sender As Object, e As EventArgs) Handles dtpCalendar.ValueChanged
        Me.LaundryBindingSource.Filter = "DateAccept = '" & dtpCalendar.Text & "'"
    End Sub

    Private Sub dgdSearch_CellMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgdSearch.CellMouseDoubleClick

        dgdSearch.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    End Sub
End Class