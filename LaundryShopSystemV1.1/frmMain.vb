﻿Imports System.Data.OleDb
Imports Microsoft.Office.Interop
Public Class frmMain
    Dim dbPath As String = Application.StartupPath & "\LaundryShop.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath
    Private Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing
        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
        Return Nothing
    End Function
    Private Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try
        Return True
    End Function
    Private Function clearFields()
        txtName.Clear()
        txtAddress.Clear()
        txtMobile.Clear()
        txtNoClothes.Clear()
        txtWeClothes.Clear()
        lblPrice.Text = ""
    End Function
    Private Sub txtWeClothes_TextChanged(sender As Object, e As EventArgs) Handles txtWeClothes.TextChanged
        Try
            If txtWeClothes.Text.Length > 0 Then
                lblPrice.Text = CDbl(txtWeClothes.Text) * 25
            Else
                lblPrice.Text = String.Empty
            End If
        Catch
        End Try
    End Sub
    Private Sub txtWeClothes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWeClothes.KeyPress
        If Not Char.IsControl(e.KeyChar) AndAlso Not Char.IsDigit(e.KeyChar) AndAlso e.KeyChar <> "."c Then
            e.Handled = True
        End If
        If e.KeyChar = "."c AndAlso TryCast(sender, TextBox).Text.IndexOf("."c) > -1 Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtNoClothes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNoClothes.KeyPress
        If Not Char.IsControl(e.KeyChar) AndAlso Not Char.IsDigit(e.KeyChar) AndAlso e.KeyChar <> "."c Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtMobile_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMobile.KeyPress
        If Not Char.IsControl(e.KeyChar) AndAlso Not Char.IsDigit(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtName.KeyPress
        If Not Char.IsControl(e.KeyChar) AndAlso Not Char.IsLetter(e.KeyChar) 
            e.Handled = True
        End If
    End Sub
    Private Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        clearFields()
        frmLogin.Show()
        Me.Hide()
        frmLogin.txtUsername.Clear()
        frmLogin.txtPassword.Clear()
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Me.Close()
        End
    End Sub
    Private Sub tmrSync_Tick(sender As Object, e As EventArgs) Handles tmrSync.Tick
        lblTime.Text = TimeOfDay
        lblDate.Text = Date.Today
    End Sub
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tmrSync.Enabled = True
    End Sub
    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        clearFields()
    End Sub
    Private Sub btbPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        If txtMobile.Text.Trim.Length = 0 Then
            MessageBox.Show("Please type a valid Mobile Number.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
                If txtName.Text.Trim.Length = 0 Then
                    MessageBox.Show("Please type your name.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
                If txtNoClothes.Text.Trim.Length = 0 Then
                    MessageBox.Show("Please type the Number Of Clothes.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
                If txtWeClothes.Text.Trim.Length = 0 Then
                    MessageBox.Show("Please enter the weight of clothes.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
                If txtAddress.Text.Trim.Length = 0 Then
                    MessageBox.Show("Please type a valid Address.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing
                Dim sqlCommand As String = "SELECT * FROM Laundry WHERE Mobile = '" & txtMobile.Text & "'"
                dbDataReader = performQuery(connectionString, sqlCommand)
        Dim wd As Word.Application
        Dim wdDoc As Word.Document
        wd = New Word.Application
        wd.Visible = True
        sqlCommand = "INSERT INTO Laundry (CoName, Address, Mobile, No_Clothes, Weight_Clothes, Status, Price, DateAccept, TimeAccept) VALUES ('" & txtName.Text & "','" & txtAddress.Text & "','" & txtMobile.Text & "','" & txtNoClothes.Text & "','" & txtWeClothes.Text & "','" & cbxStatus.Text & "','" & lblPrice.Text & "','" & lblDate.Text & "','" & lblTime.Text & "')"
        wdDoc = wd.Documents.Add(Application.StartupPath & "\RECEIPT.dotx")
        With wdDoc
            .FormFields("txtPrintName").Result = txtName.Text
            .FormFields("txtPrintNoClothes").Result = txtNoClothes.Text
            .FormFields("txtPrintWeClothes").Result = txtWeClothes.Text
            .FormFields("txtPrintMobile").Result = txtMobile.Text
            .FormFields("txtPrintAddress").Result = txtAddress.Text
            .FormFields("txtPrintDate").Result = lblDate.Text
            .FormFields("txtPrintTime").Result = lblTime.Text
            .FormFields("txtPrintPrice").Result = lblPrice.Text

            .FormFields("txtPrintName2").Result = txtName.Text
            .FormFields("txtPrintNoClothes2").Result = txtNoClothes.Text
            .FormFields("txtPrintWeClothes2").Result = txtWeClothes.Text
            .FormFields("txtPrintDate2").Result = lblDate.Text
            .FormFields("txtPrintTime2").Result = lblTime.Text
            .FormFields("txtPrintPrice2").Result = lblPrice.Text
        End With

        wdDoc.PrintOut()
        'PLEASE CHANGE THE NAME "Neamous" TO THE NAME OF YOUR COMPUTER
        wdDoc.SaveAs("Desktop")
        wd.Application.Quit()
        wd = Nothing
        If performNonQuery(connectionString, sqlCommand) Then
            clearFields()
            Exit Sub
        Else
            MessageBox.Show("Unable to Save Laundry.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtName.Focus()
            Exit Sub
        End If
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        clearFields()
        frmSearch.Show()
        Me.Hide()
    End Sub
    Private Sub btnChangePass_Click(sender As Object, e As EventArgs) Handles btnChangePass.Click
        clearFields()
        frmChangepass.Show()
        Me.Hide()
    End Sub
End Class