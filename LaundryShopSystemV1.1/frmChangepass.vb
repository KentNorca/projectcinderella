﻿Public Class frmChangepass
    Private Function clearfields()
        txtEnterPass.Clear()
        txtNewPass.Clear()
    End Function
    Private Sub Button1_Click(sender As Object, e As EventArgs)
        frmLogin.Show()
        Me.Hide()
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs)
        Me.Close()
        End
    End Sub
    Private Sub btnConfirm_Click(sender As Object, e As EventArgs) Handles btnConfim.Click
        If txtEnterPass.Text.Trim.Length = 0 And txtNewPass.Text.Trim.Length = 0 Then
            MessageBox.Show("PLEASE ENTER NEW PASSWORD.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If txtEnterPass.Text = txtNewPass.Text Then
            My.Settings.pass = txtNewPass.Text
            My.Settings.Save()
            frmLogin.Show()
            Me.Hide()
        Else
            MsgBox("PASSWORD DO NOT MATCH")
        End If
        txtEnterPass.Clear()
        txtNewPass.Clear()
        frmLogin.txtPassword.Clear()
        frmLogin.txtUsername.Clear()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        frmMain.Show()
        Me.Hide()
        txtEnterPass.Clear()
        txtNewPass.Clear()

    End Sub
End Class